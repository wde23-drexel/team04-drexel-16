package team4.spotcheckv04.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

/**
 * Created by Ziyi on 5/8/2016.
 */
public class SlidingMenu extends android.widget.HorizontalScrollView {
    private LinearLayout mWapper;
    private ViewGroup mContent;
    private ViewGroup mMenu;
    private int mScreenWidth;
    private int mMenuRightPadding = 80 ; // distance from the menu to the right of screen   dp
    private int mMenuWidth;

    private boolean once = false;
    private boolean isOpen = false; // check if the menu is open


    public SlidingMenu (Context context, AttributeSet attrs)
    {
        super(context,attrs);
        WindowManager wm= (WindowManager)context.getSystemService(context.WINDOW_SERVICE); // get the width of screen
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        mScreenWidth = outMetrics.widthPixels;
        // translate the dp to px
        mMenuRightPadding = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,80,context.getResources().getDisplayMetrics());

    }

    /*
      decide the width
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if(!once) { // make sure the function is called once
            mWapper = (LinearLayout) getChildAt(0);
            mMenu = (ViewGroup) mWapper.getChildAt(0);
            mContent = (ViewGroup) mWapper.getChildAt(1);

            mMenuWidth = mMenu.getLayoutParams().width = mScreenWidth - mMenuRightPadding; // set the width of menu
            mContent.getLayoutParams().width = mScreenWidth; // set the width of the content;
            once = true;
        }
        super.onMeasure(widthMeasureSpec,heightMeasureSpec);
    }

    @Override
    // hiding the menu
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if(changed) {
            this.scrollTo(mMenuWidth, 0); // hide the menu
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        int action = ev.getAction();
        switch (action)
        {
            case MotionEvent.ACTION_UP:
                int scrollX = getScrollX();
                /*
                 * if the hiding part of the menu is greater than the half width, we hide the menu
                 * else we show the menu up
                 */
                if(scrollX >= mMenuWidth / 2){
                    this.smoothScrollTo(mMenuWidth,0);
                    isOpen = false; // set the menu is close
                }else{
                    this.smoothScrollTo(0,0);
                    isOpen = true;
                }
                return true;
        }
        return super.onTouchEvent(ev);
    }
    public void openMenu() // open the menu
    {
        if(isOpen)return;// if the menu is open, the function returns;
        this.smoothScrollTo(0,0);
        isOpen = true;
    }
    public void closeMenu() // close the menu
    {
        if(!isOpen)return; // if the menu is close, the function returns;
        this.smoothScrollTo(mMenuWidth,0);
        isOpen = false;
    }
    public void toggle() // switch of menu
    {
        if(isOpen){
            closeMenu();
        }else{
            openMenu();
        }
    }

}
